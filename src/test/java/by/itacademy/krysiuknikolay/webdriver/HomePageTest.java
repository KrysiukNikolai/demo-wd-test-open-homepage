package by.itacademy.krysiuknikolay.webdriver;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    @Test
    public void testOnlinerOpen() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onliner.by/");
        WebElement logo = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/header/div[3]/div/div[1]/a/img"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();
    }

    @Test
    public void testAmazonOpen() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");
        WebElement logo = driver.findElement(By.xpath("/html/body/div[1]/header/div/div[1]/div[1]/div[1]/a"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();

    }
    @Test
    public void testTickedproOpen() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.ticketpro.by/");
        WebElement logo = driver.findElement(By.xpath("/html/body/div[2]/header/div/div[1]/div[1]/div/a/img"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();
    }
    @Test
    public void testAlatantoutOpen() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://alatantour.by/");
        WebElement logo = driver.findElement(By.xpath("/html/body/header/div/div/div/div/a/img"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();
    }
    @Test
    public void testOlxOpen() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.olx.pl/");
        WebElement logo = driver.findElement(By.xpath("//*[@id='headerLogo']/span[2]"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();
    }
    @Test
    public void testTripadvisorTest() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.tripadvisor.com/");
        WebElement logo = driver.findElement(By.xpath("//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img"));
        Assert.assertTrue(logo.isDisplayed());
        Thread.sleep(3000);
        driver.quit();
    }
}
